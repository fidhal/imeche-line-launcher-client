#include <ezButton.h>

// Stepper Motor Settings
#define angleStepperMotorDirPin 2
#define angleStepperMotorStepPin 3
#define springStepperMotorDirPin 4
#define springStepperMotorStepPin 5

const int angleStepperMotorEnablePin = A4;
const int springStepperMotorEnablePin = A5;

#define stepsPerRevolution 200

// EM Lock Settings
#define emLockPin 8

// Limit Switch Settings
ezButton angleLimitSwitch(A0);
ezButton springLimitSwitch(A1);

// LED Settings
const int redLEDPin = A2;
const int greenLEDPin = A3;

//

bool isSettingUpMotors = true;
bool pushAngleStepperMotor = false;
bool pushSpringStepperMotor = false;
bool angleStepperMotorSet = false;
bool springStepperMotorSet = false;

int angleStepperMotorSteps = 0;
int springStepperMotorSteps = 0;
int stepperMotorSpeed = 10000;

int angleOtherDirectionSpinCounter = 300;
int springOtherDirectionSpinCounter = 300;

int stage = 10;


void setup() {
  Serial.begin(9600);

  angleLimitSwitch.setDebounceTime(50);
  springLimitSwitch.setDebounceTime(50);

  angleStepperMotorSteps = 20;
  springStepperMotorSteps = 20;
  
  pinMode(angleStepperMotorDirPin, OUTPUT);
  pinMode(angleStepperMotorStepPin, OUTPUT);
  pinMode(springStepperMotorDirPin, OUTPUT);
  pinMode(springStepperMotorStepPin, OUTPUT);
  pinMode(angleStepperMotorEnablePin, OUTPUT);
  pinMode(springStepperMotorEnablePin, OUTPUT);
  
  pinMode(redLEDPin, OUTPUT);    
  pinMode(greenLEDPin, OUTPUT);  
  
  digitalWrite(angleStepperMotorDirPin, HIGH);
  digitalWrite(springStepperMotorDirPin, HIGH);

  digitalWrite(angleStepperMotorEnablePin, HIGH);
  digitalWrite(springStepperMotorEnablePin, HIGH);

}

// 10 Both spinning indefinetly
// 11 angleLimit hit, so spring spin indefinetly, angle spinning set amount.
// 12 springLimit hit, so angle spin indefinelty, sping spinning set amount.
// 13 both spinning set amount.
// 14 angledone spring infinete
// 15 sprindone, angle infinniete
// 16 angledone, spring set
// 17 springdone, angle set
// 18 both done

void loop() {
  angleLimitSwitch.loop();
  springLimitSwitch.loop();
  
  switch (stage){
    case 10:
        stepBothStepperMotors();
      
        if(angleLimitSwitch.isPressed()){
          digitalWrite(angleStepperMotorDirPin, LOW); 

          stage = 11;

          angleOtherDirectionSpinCounter = 300;
        }
      
        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, LOW); 

          stage = 12;

          springOtherDirectionSpinCounter = 300;
        }
       
        break;
    case 11:
        stepBothStepperMotors();

        angleOtherDirectionSpinCounter--;

        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, LOW); 

          stage = 13;

          springOtherDirectionSpinCounter = 300;
          
        }

        if(angleOtherDirectionSpinCounter == 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);
          
          stage = 14;
        }
    

        break;
    case 12:
       stepBothStepperMotors();

        springOtherDirectionSpinCounter--;

        if(angleLimitSwitch.isPressed()) {
          digitalWrite(angleStepperMotorDirPin, LOW); 

          stage = 13;

          angleOtherDirectionSpinCounter = 300;
          
        }

        if(springOtherDirectionSpinCounter == 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          
          stage = 15;
        }
   
        break;
    case 13:
        
       stepBothStepperMotors();

        angleOtherDirectionSpinCounter--;
        springOtherDirectionSpinCounter--;

        if(angleOtherDirectionSpinCounter == 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);
          
          stage = 16;
        }

        if(springOtherDirectionSpinCounter == 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          
          stage = 17;
        }
   

        break;
    case 14:
        stepSpringStepperMotor();
      
        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, LOW); 

          stage = 16;

          springOtherDirectionSpinCounter = 300;
        }
        break;
    case 15:
        stepAngleStepperMotor();
      
        if(angleLimitSwitch.isPressed()){
          digitalWrite(angleStepperMotorDirPin, LOW); 

          stage = 17;

          angleOtherDirectionSpinCounter = 300;
        }
        break;
    case 16:
        stepSpringStepperMotor();

        springOtherDirectionSpinCounter--;

        if(springOtherDirectionSpinCounter == 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);

          stage = 18;
        }
        
        break;
    case 17:
        stepAngleStepperMotor();

        angleOtherDirectionSpinCounter--;

        if(angleOtherDirectionSpinCounter == 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);

          stage = 18;
        }
        
        break;
    case 18:

        digitalWrite(redLEDPin, HIGH);
        digitalWrite(greenLEDPin, HIGH);
        break;
  }

}

void stepBothStepperMotors(){     
    digitalWrite(angleStepperMotorEnablePin, LOW);
    digitalWrite(springStepperMotorEnablePin, LOW);
    
    digitalWrite(angleStepperMotorStepPin, HIGH);
    digitalWrite(springStepperMotorStepPin, HIGH);
    delayMicroseconds(5000);
    digitalWrite(angleStepperMotorStepPin, LOW);
    digitalWrite(springStepperMotorStepPin, LOW);
    delayMicroseconds(5000);
}

void stepSpringStepperMotor(){
    digitalWrite(springStepperMotorEnablePin, LOW);
    
    digitalWrite(springStepperMotorStepPin, HIGH);
    delayMicroseconds(5000);
    digitalWrite(springStepperMotorStepPin, LOW);
    delayMicroseconds(5000);
}

void stepAngleStepperMotor(){ 
    digitalWrite(angleStepperMotorEnablePin, LOW);
    
    digitalWrite(angleStepperMotorStepPin, HIGH);
    delayMicroseconds(5000);
    digitalWrite(angleStepperMotorStepPin, LOW);
    delayMicroseconds(5000);
}
