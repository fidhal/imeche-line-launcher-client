#include <ezButton.h>

// Stepper Motor Settings
#define angleStepperMotorDirPin 2
#define angleStepperMotorStepPin 3
#define springStepperMotorDirPin 4
#define springStepperMotorStepPin 5
#define stepsPerRevolution 200

// EM Lock Settings
#define emLockPin 8

// Limit Switch Settings
ezButton angleLimitSwitch(A0);
ezButton springLimitSwitch(A1);

// LED Settings
const int redLEDPin = A2;
const int greenLEDPin = A3;

//

bool isSettingUpMotors = true;
bool pushAngleStepperMotor = false;
bool pushSpringStepperMotor = false;
bool angleStepperMotorSet = false;
bool springStepperMotorSet = false;

int angleStepperMotorSteps = 0;
int springStepperMotorSteps = 0;
int stepperMotorSpeed = 10000;


void setup() {
  Serial.begin(9600);

  angleLimitSwitch.setDebounceTime(50);
  springLimitSwitch.setDebounceTime(50);

  angleStepperMotorSteps = 20;
  springStepperMotorSteps = 20;

  
  // Declare pins as output:
  pinMode(angleStepperMotorDirPin, OUTPUT);
  pinMode(angleStepperMotorStepPin, OUTPUT);
  pinMode(springStepperMotorDirPin, OUTPUT);
  pinMode(springStepperMotorStepPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);    
  pinMode(greenLEDPin, OUTPUT);  
  

}

void loop() {
  if(isSettingUpMotors == true) {
    setUpMotors();
    
  }
  

}


void setUpMotors() {

  digitalWrite(angleStepperMotorDirPin, HIGH);
  digitalWrite(springStepperMotorDirPin, HIGH);
  
  if(pushAngleStepperMotor == true){
    for (int i = 0; i < 200; i++) {
      digitalWrite(angleStepperMotorStepPin, HIGH); 
      delayMicroseconds(stepperMotorSpeed);
      digitalWrite(angleStepperMotorStepPin, LOW); 
      delayMicroseconds(stepperMotorSpeed);
    }
    
    Serial.println("tryna push angle stepper motor");  
  }
  if(pushSpringStepperMotor == true){
    digitalWrite(springStepperMotorStepPin, HIGH);    
    Serial.println("tryna push spring stepper motor");  
  }
  
  
  
  if(pushAngleStepperMotor == true){
    digitalWrite(angleStepperMotorStepPin, LOW);
    angleStepperMotorSteps = --angleStepperMotorSteps;
    Serial.println("tryna unpush angle stepper motor"); 
  }
  if(pushSpringStepperMotor == true){
    digitalWrite(springStepperMotorStepPin, LOW);    
    springStepperMotorSteps = --springStepperMotorSteps;
    Serial.println("tryna unpush spring stepper motor");  
  }
  
  delayMicroseconds(5000);

  if(angleStepperMotorSteps == 0 && angleStepperMotorSet == false){
    pushAngleStepperMotor = false;
    angleStepperMotorSet = true;    
    Serial.println("angle stepper motor steps 0");  
  }
  if(springStepperMotorSteps == 0 && springStepperMotorSet == false){
    pushSpringStepperMotor = false;
    springStepperMotorSet = true;
    Serial.println("spring stepper motor steps 0");  
  }

  if(angleStepperMotorSteps == 0 && springStepperMotorSteps == 0){
    isSettingUpMotors = false;
  }

  if(pushAngleStepperMotor == false && angleStepperMotorSet == false) {
    angleLimitSwitch.loop();
  
    int angleLimitSwitchState = angleLimitSwitch.getState();
    if(angleLimitSwitchState == HIGH){
      Serial.println("AngleLimitSwitch Pressed");
      pushAngleStepperMotor = true;
    }
    
  }

  if(pushSpringStepperMotor == false && springStepperMotorSet == false) {
    springLimitSwitch.loop();
    
    int springLimitSwitchState = springLimitSwitch.getState();
    if(springLimitSwitchState == HIGH){
      Serial.println("SpringLimitSwitch Pressed");
      pushSpringStepperMotor = true;
    }
  }



  
  
}
