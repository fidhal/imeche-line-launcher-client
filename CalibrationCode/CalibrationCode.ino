#include <ezButton.h>

// Stepper Motor Settings
#define angleStepperMotorDirPin 2
#define angleStepperMotorStepPin 3
#define springStepperMotorDirPin 4
#define springStepperMotorStepPin 5

const int angleStepperMotorEnablePin = A4;
const int springStepperMotorEnablePin = A5;

#define stepsPerRevolution 200

// EM Lock Settings
#define emLockPin 8

// Limit Switch Settings
ezButton angleLimitSwitch(A0);
ezButton springLimitSwitch(A1);

// LED Settings
const int redLEDPin = A2;
const int greenLEDPin = A3;

//

bool isSettingUpMotors = true;
bool pushAngleStepperMotor = false;
bool pushSpringStepperMotor = false;
bool angleStepperMotorSet = false;
bool springStepperMotorSet = false;

int angleStepperMotorSteps = 0;
int springStepperMotorSteps = 0;
int stepperMotorSpeed = 10000;

int otherDirectionSpinCounter = 300;

int stage = 1;


void setup() {
  Serial.begin(9600);

  angleLimitSwitch.setDebounceTime(50);
  springLimitSwitch.setDebounceTime(50);

  angleStepperMotorSteps = 20;
  springStepperMotorSteps = 20;
  
  // Declare pins as output:
  pinMode(angleStepperMotorDirPin, OUTPUT);
  pinMode(angleStepperMotorStepPin, OUTPUT);
  pinMode(springStepperMotorDirPin, OUTPUT);
  pinMode(springStepperMotorStepPin, OUTPUT);
  pinMode(angleStepperMotorEnablePin, OUTPUT);
  pinMode(springStepperMotorEnablePin, OUTPUT);
  
  pinMode(redLEDPin, OUTPUT);    
  pinMode(greenLEDPin, OUTPUT);  
  
  digitalWrite(angleStepperMotorDirPin, HIGH);
  digitalWrite(springStepperMotorDirPin, HIGH);

  digitalWrite(angleStepperMotorEnablePin, HIGH);
  digitalWrite(springStepperMotorEnablePin, HIGH);

  digitalWrite(redLEDPin, HIGH);

}

void loop() {
  angleLimitSwitch.loop();
  springLimitSwitch.loop();
  
  switch (stage){
    case 1:
        
        digitalWrite(angleStepperMotorEnablePin, LOW);
        digitalWrite(angleStepperMotorStepPin, HIGH);
        delayMicroseconds(5000);
        digitalWrite(angleStepperMotorStepPin, LOW);
        delayMicroseconds(5000);
      
        if(angleLimitSwitch.isPressed()){
          digitalWrite(angleStepperMotorDirPin, LOW); 

          otherDirectionSpinCounter = 300;

          stage++;          
        }
       
        break;
    case 2:
    
        digitalWrite(angleStepperMotorEnablePin, LOW);
        // These four lines result in 1 step:
        digitalWrite(angleStepperMotorStepPin, HIGH);
        //digitalWrite(m2StepPin, HIGH);
        delayMicroseconds(10000);
        digitalWrite(angleStepperMotorStepPin, LOW);
        //digitalWrite(m2StepPin, LOW);
        delayMicroseconds(10000);

        otherDirectionSpinCounter--;

        if(otherDirectionSpinCounter == 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);
          digitalWrite(redLEDPin, LOW);
          digitalWrite(greenLEDPin, HIGH);
          
          stage++;
        }
        break;
    case 3:
        digitalWrite(springStepperMotorEnablePin, LOW);
        digitalWrite(springStepperMotorStepPin, HIGH);
        delayMicroseconds(5000);
        digitalWrite(springStepperMotorStepPin, LOW);
        delayMicroseconds(5000);

        if(springLimitSwitch.isPressed()){
          
          digitalWrite(springStepperMotorDirPin, LOW);

          
          otherDirectionSpinCounter = 300;
          
          stage++;
        }
        break;
    case 4:
        digitalWrite(springStepperMotorEnablePin, LOW);
        digitalWrite(springStepperMotorStepPin, HIGH);
        delayMicroseconds(5000);
        digitalWrite(springStepperMotorStepPin, LOW);
        delayMicroseconds(5000);


        otherDirectionSpinCounter--;

        if(otherDirectionSpinCounter == 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(greenLEDPin, LOW);
          
          stage++;
        }
        break;
  }

}
