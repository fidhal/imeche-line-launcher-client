#include <ezButton.h>


int redLEDPin = 2;

ezButton limitSwitch(7);  // create ezButton object that attach to pin 7;

void setup() {
  Serial.begin(9600);
  pinMode(redLEDPin, OUTPUT);    
  limitSwitch.setDebounceTime(50); // set debounce time to 50 milliseconds
}

void loop() {
  limitSwitch.loop(); // MUST call the loop() function first

  if(limitSwitch.isPressed()){
    Serial.println("The limit switch: UNTOUCHED -> TOUCHED");
    
    digitalWrite(redLEDPin, HIGH);   // turn the LED on 
    
  }

  if(limitSwitch.isReleased()){
    
    Serial.println("The limit switch: TOUCHED -> UNTOUCHED");
    digitalWrite(redLEDPin, LOW);   // turn the LED on 
  }

  int state = limitSwitch.getState();
  if(state == HIGH)
    Serial.println("The limit switch: UNTOUCHED");
  else
    Serial.println("The limit switch: TOUCHED");
}
