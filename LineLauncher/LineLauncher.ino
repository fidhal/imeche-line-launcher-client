/* TODO
 *
 * - Incorporate how we calculating rotations
 * - Add inputting angle and initial V
 * - add keeping track of spring motor turns, to turn that back
 * - linearly change speed.
 *
 *
 */

#include <ezButton.h>

const float angleDistance1 = 10.0;
const float angleDistance2 = 15.0;
const float angleDistance3 = 20.0;

float averageAngleDistance = 50;

int angleAverageSteps;

const float springDistance1 = 10.0;
const float springDistance2 = 15.0;
const float springDistance3 = 20.0;

int angleSteps = 0;
int springSteps = 0;

int allAngleSteps[3];
int allSpringSteps[3];

const float initialVelocity1 = 10.0;
const float initialVelocity2 = 12.0;
const float initialVelocity3 = 13.0;

const int angleStepperMotorInterval = 5000;
const int springStepperMotorInterval = 750;


// Stepper Motor Settings
// Angle motor is nema 17
// Spring motor is the nema 23
#define angleStepperMotorDirPin 2
#define angleStepperMotorStepPin 3
#define springStepperMotorDirPin 4
#define springStepperMotorStepPin 5

const int angleStepperMotorEnablePin = A4;
const int springStepperMotorEnablePin = A5;

#define stepsPerRevolution 200

// EM Lock Settings
#define emLockPin 8

const int launchButtonPin = 9;
int launchButtonState = 0;
int ballsLaunched = 0;

// Emergency Button
ezButton emergencyButton(10);


// Limit Switch Settings
ezButton angleLimitSwitch(A0);
ezButton springLimitSwitch(A1);

// LED Settings
const int redLEDPin = A2;
const int greenLEDPin = A3;

//

int angleStepperMotorSteps = 0;
int springStepperMotorSteps = 0;

int angleOtherDirectionSpinCounter = 300;
int springOtherDirectionSpinCounter = 300;

int stage = 50;


void setup() {
  Serial.begin(9600);

  angleLimitSwitch.setDebounceTime(50);
  springLimitSwitch.setDebounceTime(50);
  emergencyButton.setDebounceTime(50);

  angleStepperMotorSteps = 20;
  springStepperMotorSteps = 20;

  pinMode(angleStepperMotorDirPin, OUTPUT);
  pinMode(angleStepperMotorStepPin, OUTPUT);
  pinMode(springStepperMotorDirPin, OUTPUT);
  pinMode(springStepperMotorStepPin, OUTPUT);
  pinMode(angleStepperMotorEnablePin, OUTPUT);
  pinMode(springStepperMotorEnablePin, OUTPUT);

  pinMode(redLEDPin, OUTPUT);
  pinMode(greenLEDPin, OUTPUT);

  pinMode(emLockPin, OUTPUT);

  pinMode(launchButtonPin, INPUT);

  digitalWrite(angleStepperMotorDirPin, HIGH);
  digitalWrite(springStepperMotorDirPin, LOW);

  digitalWrite(angleStepperMotorEnablePin, HIGH);
  digitalWrite(springStepperMotorEnablePin, HIGH);

  digitalWrite(redLEDPin, HIGH);
  digitalWrite(greenLEDPin, LOW);

  digitalWrite(emLockPin, LOW);


  setupAnglesAndVelocities();


}

// 10 Both spinning indefinetly
// 11 angleLimit hit, so spring spin indefinetly, angle spinning set amount.
// 12 springLimit hit, so angle spin indefinelty, sping spinning set amount.
// 13 both spinning set amount.
// 14 angledone spring infinete
// 15 sprindone, angle infinniete
// 16 angledone, spring set
// 17 springdone, angle set
// 20 both done/ ready for launch

void loop() {
  angleLimitSwitch.loop();
  springLimitSwitch.loop();
  emergencyButton.loop();

  if(emergencyButton.isPressed()){
    digitalWrite(greenLEDPin, LOW);
    stage = 999;
  }

  switch (stage){
    case 10:
        stepBothStepperMotors();

        if(angleLimitSwitch.isPressed()){
          digitalWrite(angleStepperMotorDirPin, LOW);

          stage = 11;

          angleSteps = angleAverageSteps;
        }

        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, HIGH);

          stage = 12;
        }

        break;
    case 11:
        stepBothStepperMotors();

        angleSteps--;

        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, HIGH);

          stage = 13;
        }

        if(angleSteps <= 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);

          stage = 14;
        }


        break;
    case 12:
       stepBothStepperMotors();

        springSteps--;

        if(angleLimitSwitch.isPressed()) {
          digitalWrite(angleStepperMotorDirPin, LOW);

          stage = 13;

          angleSteps = angleAverageSteps;
        }

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(emLockPin, HIGH);

          stage = 15;
        }

        break;
    case 13:
       stepBothStepperMotors();

        angleSteps--;
        springSteps--;

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(emLockPin, HIGH);

          stage = 17;
        }

        if(angleSteps <= 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);

          stage = 16;
        }


        break;
    case 14:
        stepSpringStepperMotor();

        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, HIGH);

          stage = 16;
        }
        break;
    case 15:
        stepAngleStepperMotor();

        if(angleLimitSwitch.isPressed()){
          digitalWrite(angleStepperMotorDirPin, LOW);

          stage = 17;

          angleSteps = 300;
        }
        break;
    case 16:
        stepSpringStepperMotor();

        springSteps--;

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(redLEDPin, LOW);
          digitalWrite(greenLEDPin, HIGH);
          digitalWrite(emLockPin, HIGH);

          stage = 20;
        }

        break;
    case 17:
        stepAngleStepperMotor();

        angleSteps--;

        if(angleSteps == 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);
          digitalWrite(redLEDPin, LOW);
          digitalWrite(greenLEDPin, HIGH);

          stage = 20;
        }

        break;
    case 20:
        launchButtonState = digitalRead(launchButtonPin);

        if (launchButtonState == HIGH) {

          if (angleAverageSteps > allAngleSteps[ballsLaunched]){
            digitalWrite(angleStepperMotorDirPin, HIGH);
          } else if (angleAverageSteps < allAngleSteps[ballsLaunched]) {
            digitalWrite(angleStepperMotorDirPin, LOW);
          }

          angleSteps = angleAverageSteps - allAngleSteps[ballsLaunched];

          springSteps = allSpringSteps[ballsLaunched];
          digitalWrite(springStepperMotorDirPin, HIGH);

          digitalWrite(redLEDPin, HIGH);
          digitalWrite(greenLEDPin, LOW);

          stage = 21;
        }

        break;
    case 21:
       stepBothStepperMotors();

        angleSteps--;
        springSteps--;

        if(angleSteps <= 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);

          stage = 22;
        }

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);

          stage = 23;
        }

        break;
    case 22:
        stepSpringStepperMotor();

        springSteps--;

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(emLockPin, LOW);
          delay(2000);
          angleSteps = angleAverageSteps - allAngleSteps[ballsLaunched];

          digitalWrite(angleStepperMotorDirPin, !digitalRead(angleStepperMotorDirPin));
          digitalWrite(springStepperMotorDirPin, LOW);

          stage = 24;
        }

        break;
    case 23:
        stepAngleStepperMotor();

        angleSteps--;

        if(angleSteps <= 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);
          digitalWrite(emLockPin, LOW);
          delay(2000);
          angleSteps = angleAverageSteps - allAngleSteps[ballsLaunched];

          digitalWrite(angleStepperMotorDirPin, !digitalRead(angleStepperMotorDirPin));
          digitalWrite(springStepperMotorDirPin, LOW);

          stage = 24;
        }

        break;
    case 24:
        stepBothStepperMotors();

        angleSteps--;

        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, HIGH);
          digitalWrite(springStepperMotorEnablePin, HIGH);

          stage = 25;

          springSteps = 5;

        }

        if(angleSteps <= 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);

          stage = 26;
        }


        break;
    case 25:
       stepBothStepperMotors();

        angleSteps--;
        springSteps--;

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(emLockPin, HIGH);

          stage = 28;
        }

        if(angleSteps <= 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);

          stage = 27;
        }


        break;
    case 26:
        stepSpringStepperMotor();

        if(springLimitSwitch.isPressed()){
          digitalWrite(springStepperMotorDirPin, HIGH);
          digitalWrite(springStepperMotorEnablePin, HIGH);

          stage = 27;

          springSteps = 5;
        }
        break;
    case 27:
        stepSpringStepperMotor();

        springSteps--;

        if(springOtherDirectionSpinCounter <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);
          digitalWrite(redLEDPin, LOW);
          digitalWrite(greenLEDPin, HIGH);

          ballsLaunched++;

          if(ballsLaunched >= 3){
            stage = 30;
          }
          else{
            digitalWrite(angleStepperMotorDirPin, LOW);
            digitalWrite(springStepperMotorDirPin, HIGH);
            stage = 20;

          }

        }

        break;
    case 28:
        stepAngleStepperMotor();

        angleSteps--;

        if(angleSteps == 0){
          digitalWrite(angleStepperMotorEnablePin, HIGH);
          digitalWrite(redLEDPin, LOW);
          digitalWrite(greenLEDPin, HIGH);

          ballsLaunched++;

          if(ballsLaunched >= 3){
            stage = 30;
          }
          else{
            digitalWrite(angleStepperMotorDirPin, LOW);
            digitalWrite(springStepperMotorDirPin, HIGH);
            stage = 20;

          }
        }

        break;
    case 30:
        digitalWrite(redLEDPin, LOW);
        digitalWrite(greenLEDPin, LOW);
        delay(100);
        digitalWrite(greenLEDPin, HIGH);
        delay(100);
        break;
    case 50:
        launchButtonState = digitalRead(launchButtonPin);

        if (launchButtonState == HIGH) {
          digitalWrite(emLockPin, HIGH);

          springSteps = allSpringSteps[ballsLaunched];
          digitalWrite(springStepperMotorDirPin, HIGH);

          digitalWrite(redLEDPin, HIGH);
          digitalWrite(greenLEDPin, LOW);

          stage = 51;
        break;
    case 51:
        stepSpringStepperMotor();

        springSteps--;

        if(springSteps <= 0){
          digitalWrite(springStepperMotorEnablePin, HIGH);

          digitalWrite(emLockPin, LOW);
          delay(3000);

          digitalWrite(springStepperMotorDirPin, LOW);
          springSteps = allSpringSteps[ballsLaunched];

          stage = 52;
        }

        break;
    case 52:
        stepSpringStepperMotor();

        springSteps--;

        if(springSteps <= 0){
            digitalWrite(greenLEDPin, HIGH);
            digitalWrite(redLEDPin, LOW);

            if(ballsLaunched >= 3){
                stage = 30;
              } else{
              digitalWrite(springStepperMotorDirPin, HIGH);
              stage = 50;
            }
        }

        break;

    case 999:

        digitalWrite(redLEDPin, HIGH);
        delay(100);
        digitalWrite(redLEDPin, LOW);
        delay(100);
        break;
  }

}

}


void setupAnglesAndVelocities(){
  averageAngleDistance = (angleDistance1 + angleDistance2+ angleDistance3)/3;

  if ((averageAngleDistance == angleDistance1) || averageAngleDistance == angleDistance2 || averageAngleDistance == angleDistance3){
    averageAngleDistance = averageAngleDistance + 5;
  }

  angleAverageSteps = angleDistanceToSteps(averageAngleDistance);
  springSteps = 5;

  allAngleSteps[0] = angleDistanceToSteps(angleDistance1);
  allAngleSteps[1] = angleDistanceToSteps(angleDistance2);
  allAngleSteps[2] = angleDistanceToSteps(angleDistance3);

  allSpringSteps[0] = springDistanceToSteps(springDistance1);
  allSpringSteps[1] = springDistanceToSteps(springDistance2);
  allSpringSteps[2] = springDistanceToSteps(springDistance3);
}

int angleDistanceToSteps(float angleDistance){
  int steps;

  steps = round(angleDistance * 25);

  if(steps > 200){
    steps = steps - 50;
  }

  return steps;
}

int springDistanceToSteps(float springDistance){
  int steps;

  steps = round(springDistance * 100);

  if(steps > 800){
    steps = steps - 200;
  }

  return steps;
}

int calculateAngleStepperMotorSteps(){
  return 200;

}

int calculateSpringStepperMotorSteps(){
  return 250;

}

void stepBothStepperMotors(){
    digitalWrite(angleStepperMotorEnablePin, LOW);
    digitalWrite(springStepperMotorEnablePin, LOW);

    digitalWrite(angleStepperMotorStepPin, HIGH);

      for (int i = 0; i < 4; i++) {
        stepSpringStepperMotor();
      }
    digitalWrite(angleStepperMotorStepPin, LOW);

      for (int i = 0; i < 4; i++) {
        stepSpringStepperMotor();
      }
}

void stepSpringStepperMotor(){
    digitalWrite(springStepperMotorEnablePin, LOW);

    digitalWrite(springStepperMotorStepPin, HIGH);
    delayMicroseconds(springStepperMotorInterval);
    digitalWrite(springStepperMotorStepPin, LOW);
    delayMicroseconds(springStepperMotorInterval);
}

void stepAngleStepperMotor(){
    digitalWrite(angleStepperMotorEnablePin, LOW);

    digitalWrite(angleStepperMotorStepPin, HIGH);
    delayMicroseconds(angleStepperMotorInterval);
    digitalWrite(angleStepperMotorStepPin, LOW);
    delayMicroseconds(angleStepperMotorInterval);
}
