#include <ezButton.h>

/*Example sketch to control a stepper motor with A4988 stepper motor driver and Arduino without a library. More info: https://www.makerguides.com */

// Define stepper motor connections and steps per revolution:
#define m1DirPin 2
#define m1StepPin 3
#define m2DirPin 4
#define m2StepPin 5
#define stepsPerRevolution 200
#define emLockPin 8   // the Arduino pin, which connects to the IN pin of relay



const int angleStepperMotorEnablePin = A4;
const int springStepperMotorEnablePin = A5;

const int redLEDPin = A2;
int greenLEDPin = 7;

ezButton limitSwitchPin(A0);

void setup() {
  // Declare pins as output:
  pinMode(m1DirPin, OUTPUT);
  pinMode(m1StepPin, OUTPUT);
  pinMode(m2DirPin, OUTPUT);
  pinMode(m2StepPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);    
  pinMode(greenLEDPin, OUTPUT);    
  pinMode(emLockPin, OUTPUT);

  
  pinMode(angleStepperMotorEnablePin, OUTPUT);
  pinMode(springStepperMotorEnablePin, OUTPUT);
  

  digitalWrite(angleStepperMotorEnablePin, HIGH);
  digitalWrite(springStepperMotorEnablePin, HIGH);

  limitSwitchPin.setDebounceTime(50);


  // Set the spinning direction clockwise:
  digitalWrite(m1DirPin, HIGH);
  digitalWrite(m2DirPin, LOW);

  digitalWrite(angleStepperMotorEnablePin, LOW);
  digitalWrite(springStepperMotorEnablePin, LOW);

  int steps = (5 * 200) - 50;
  
  for (int i = 0; i < steps; i++) {
    // These four lines result in 1 step:
    digitalWrite(m1StepPin, HIGH);
    //digitalWrite(m2StepPin, HIGH);
    delayMicroseconds(2000);
    digitalWrite(m1StepPin, LOW);
    //digitalWrite(m2StepPin, LOW);
    delayMicroseconds(2000);
  }

  
  digitalWrite(angleStepperMotorEnablePin, HIGH);
  digitalWrite(springStepperMotorEnablePin, HIGH);
}

void loop() {

  return;

  // Set the spinning direction clockwise:
  digitalWrite(m1DirPin, HIGH);
  digitalWrite(m2DirPin, HIGH);
  
  digitalWrite(redLEDPin, HIGH);   // turn the LED on 
  digitalWrite(greenLEDPin, LOW);   // turn the LED on 

  for (int i = 0; i < 5 * stepsPerRevolution; i++) {
    // These four lines result in 1 step:
    digitalWrite(m1StepPin, HIGH);
    //digitalWrite(m2StepPin, HIGH);
    delayMicroseconds(20000);
    digitalWrite(m1StepPin, LOW);
    //digitalWrite(m2StepPin, LOW);
    delayMicroseconds(20000);
  }
  
  digitalWrite(redLEDPin, LOW);    // turn the LED off 
  digitalWrite(greenLEDPin, HIGH);   // turn the LED on 

  delay(2000);
}
